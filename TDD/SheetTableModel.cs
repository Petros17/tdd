﻿using System;
using System.Collections.Generic;

namespace TDD
{
    /// <summary>
    /// Class encapsulating sheet data.
    /// </summary>
    public class SheetTableModel
    {
        // Possible columns span between A and ZZ.
        //
        private const int MAX_COLUMN_COUNT = 26 + 26 * 26;
        private const int MAX_ROW_COUNT = 100;

        private Sheet sheet;

        /// <summary>
        /// Public constructor, with empty default sheet.
        /// </summary>
        public SheetTableModel()
        {
            sheet = new Sheet();
            this.InitializeSheet();
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="sheet">Sheet to initialize local with.</param>
        public SheetTableModel(Sheet sheet)
        {
            this.sheet = sheet;
        }

        /// <summary>
        /// Retrieves list of all cells.
        /// </summary>
        /// <returns></returns>
        public IList<Cell> GetCells()
        {
            return sheet.GetCells();
        }

        /// <summary>
        /// Retrieves number of columns. Currently hardcoded.
        /// </summary>
        /// <returns></returns>
        public int GetColumnCount()
        {
            return MAX_COLUMN_COUNT;
        }

        /// <summary>
        /// Retrieves number of rows. Currently hardcoded.
        /// </summary>
        /// <returns></returns>
        public int GetRowCount()
        {
            return MAX_ROW_COUNT;
        }

        /// <summary>
        /// Retrieves value in table on specified row and column.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public Object GetValueAt(int row, int column)
        {
            if (row > GetRowCount() || row < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Row {0} is out of bounds", row));
            }

            if (column > GetColumnCount() || column < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Column {0} is out of bounds", column));
            }

            // Return to indexing from 1 for rows.
            //
            row += 1;

            if (column == 0)
            {
                return row;
            }

            return sheet.GetCell(GetCellName(row, column));
        }

        /// <summary>
        /// Returns string literal.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public string GetLiteralValueAt(int row, int column)
        {
            if (row > GetRowCount() || row < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Row {0} is out of bounds", row));
            }

            if (column > GetColumnCount() || column < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Column {0} is out of bounds", column));
            }

            // Return to indexing from 1 for rows.
            //
            row += 1;

            if (column == 0)
            {
                return row.ToString();
            }

            return sheet.GetCellLiteral(GetCellName(row, column));
        }

        /// <summary>
        /// Sets value in the underlying sheet to the given position.
        /// </summary>
        /// <param name="row">Row.</param>
        /// <param name="column">Column.</param>
        /// <param name="value">Value to set.</param>
        public void SetValueAt(int row, int column, string value)
        {
            if (row > GetRowCount() || row < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Row {0} is out of bounds", row));
            }

            if (column > GetColumnCount() || column < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Column {0} is out of bounds", column));
            }

            // Return to indexing from 1 for rows.
            //
            row += 1;

            sheet.PutCell(GetCellName(row, column), value);
        }

        /// <summary>
        /// Retrieves name of the column with the given index.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public string GetColumnName(int column)
        {
            if (column > GetColumnCount() || column < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Column {0} is out of bounds", column));
            }

            string columnName = "";
            if (column == 0)
            {
                return "";
            }

            column -= 1;

            if (column < 26)
            {
                columnName += (char)('A' + column);
            }
            else if (column >= 26)
            {
                column -= 26;
                columnName += (char)('A' + (column / 26));
                columnName += (char)('A' + (column % 26));
            }

            return columnName;
        }

        /// <summary>
        /// Returns the index of the column, based on the name.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>Index.</returns>
        public Cell FindCell(string columnName)
        {
            return sheet.GetCellObject(columnName);
        }

        /// <summary>
        /// Retrieves actual sheet cell name based on row and column index.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="column">Column index.</param>
        /// <returns>Name of the cell.</returns>
        private string GetCellName(int row, int column)
        {
            return GetColumnName(column) + row;
        }

        /// <summary>
        /// Initialize the sheet with proper values.
        /// </summary>
        private void InitializeSheet()
        {
            for (int i = 1; i < MAX_COLUMN_COUNT; i++)
            {
                string columnName = GetColumnName(i);
                for (int j = 1; j <= MAX_ROW_COUNT; j++)
                {
                    string cellName = columnName + j;
                    sheet.PutCell(cellName, "");
                }
            }
        }
    }
}