﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace TDD
{
    /// <summary>
    /// Tokenizer for evaluating formulas. Will supply tokens to the parser.
    /// </summary>
    public class Tokenizer
    {
        /// <summary>
        /// Literal which is tokenized.
        /// </summary>
        private readonly string literal;

        /// <summary>
        /// Pattern used to regex match.
        /// </summary>
        private const string pattern = @"(\()|(\))|(\+)|(-)|(\*)|(/)";

        /// <summary>
        /// List of tokens extracted from the literal.
        /// </summary>
        private Queue<string> tokens;

        /// <summary>
        /// Gets number of tokens parsed.
        /// </summary>
        public int ParsedTokens
        {
            get;
            private set;
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="input"></param>
        public Tokenizer(string input)
        {
            literal = input;

            // Split tokens, remove empty strings, and trim.
            //
            var tokenList = new List<string>(Regex.Split(input, pattern))
                .Select(s => s.Trim())
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList();

            tokens = new Queue<string>(tokenList);
        }

        /// <summary>
        /// Basic GetNextToken implementation.
        /// </summary>
        /// <returns>Next token if any, null otherwise.</returns>
        public string GetNextToken()
        {
            string token = null;
            if (tokens.Count > 0)
            {
                token = tokens.Dequeue();
                ParsedTokens++;
            }

            return token;
        }

        /// <summary>
        /// Peeks next token.
        /// </summary>
        /// <returns>Front of the queue, if not empty.</returns>
        public string PeekNextToken()
        {
            string token = null;
            if (tokens.Count > 0)
            {
                token = tokens.Peek();
            }

            return token;
        }
    }
}
