﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using static TDD.FormulaParser;

namespace TDD
{
    /// <summary>
    /// Class representing a Cell.
    /// </summary>
    public class Cell : INotifyPropertyChanged
    {
        /// <summary>
        /// Name of the cell.
        /// </summary>
        public string CellName
        {
            get;
            private set;
        }

        /// <summary>
        /// Indicates if the value has been read at least once.
        /// </summary>
        private bool cacheDirty;

        /// <summary>
        /// Value of the cell.
        /// </summary>
        private string value;

        /// <summary>
        /// The cell literal.
        /// </summary>
        private string cellLiteral;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Literal of the cell, used for value calculation.
        /// </summary>
        public string CellLiteral
        {
            get
            {
                return cellLiteral;
            }
            set
            {
                if (value != cellLiteral)
                {
                    cellLiteral = value;
                    
                    // Notify all cells which referenced this one about the change.
                    //
                    ReferenceResolver.GetInstance().NotifyReferences(this.CellName);
                    
                    // Remove all references that this cell had.
                    //
                    ReferenceResolver.GetInstance().RemoveReferences(this.CellName); 

                    // Parse and save new references for this cell.
                    //
                    new FormulaParser().ParseReferences(this.CellName, this.CellLiteral);
                    NotifyPropertyChanged("CellLiteral");
                    NotifyPropertyChanged("CellValue");
                }
            }
        }

        /// <summary>
        /// Calculates the value of the cell.
        /// </summary>
        public string CellValue
        {
            get
            {
                if (this.cacheDirty)
                {
                    RetrieveValue();
                }

                return this.value;
            }
        }

        /// <summary>
        /// Notifies this cell to update the value.
        /// </summary>
        public void UpdateCellValue()
        {
            RetrieveValue();

            ReferenceResolver.GetInstance().NotifyReferences(this.CellName);
            NotifyPropertyChanged("CellValue");
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <param name="literal">Cell literal.</param>
        public Cell(string cellName, string literal = "")
        {
            this.CellName = cellName;
            this.CellLiteral = literal;
            this.cacheDirty = true;
        }

        /// <summary>
        /// Calculates value of the cell.
        /// </summary>
        /// <returns>Value of the cell.</returns>
        internal string GetValue()
        {       
            if (this.cacheDirty)
            {
                value = new FormulaParser().Parse(this.CellLiteral);

                if (IsNumeric(value))
                {
                    value = value.Trim();
                }
            }         

            return value;
        }

        /// <summary>
        /// Value can be set externally, in case of an error.
        /// </summary>
        /// <param name="newValue">New value.</param>
        /// <returns></returns>
        public void SetValue(string newValue)
        {
            value = newValue;
            cacheDirty = true;
        }

        /// <summary>
        /// Validate that cell name is proper.
        /// Proper cell name is A-Z{A-Z}, 1..n
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <returns>True, if name is proper, false otherwise.</returns>
        internal static bool IsCellNameValid(string cellName)
        {
            Regex regex = new Regex(@"^[a-zA-Z]{1,2}[1-9][0-9]*$");
            Match match = regex.Match(cellName);
            return match.Success;
        }

        /// <summary>
        /// Check if value is purely numeric, as a number with optional spaces before or after it.
        /// </summary>
        /// <param name="val">Value to check.</param>
        /// <returns>True, if this is a number. False, otherwise.</returns>
        internal static bool IsNumeric(string val)
        {
            Regex regex = new Regex(@"^[ ]*[0-9]+[ ]*$");
            Match match = regex.Match(val);
            return match.Success;
        }

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Retrieves value with error handling.
        /// </summary>
        private void RetrieveValue()
        {
            try
            {
                GetValue();
            }
            catch (CircularDependencyException)
            {
                value = "#Circular";
            }
            catch (Exception)
            {
                value = "#Error";
            }

            cacheDirty = true;
        }
    }
}
