﻿/**
* File containing basic Sheet class.
* 
* @author Petar Kovacev
* 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static TDD.FormulaParser;

namespace TDD
{
    /// <summary>
    /// Class representing a spreadsheet.
    /// </summary>
    public class Sheet
    {
        /// <summary>
        /// Maps cell name to Cell object.
        /// </summary>
        private readonly Dictionary<string, Cell> cells;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Sheet()
        {
            cells = new Dictionary<string, Cell>();
            ReferenceResolver.GetInstance().Sheet = this;
        }

        /// <summary>
        /// Retrieve cell object.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <returns></returns>
        internal Cell GetCellObject(string cellName)
        {
            return GetCellFromDictionary(cellName);
        }

        /// <summary>
        /// Returns cell for the given name.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <returns>Cell value./returns>
        public string GetCell(string cellName)
        {
            Cell cell = GetCellFromDictionary(cellName);

            string returnValue = "";

            try
            {
                if (cell != null)
                {
                    returnValue = cell.GetValue();
                }
            }
            catch (CircularDependencyException)
            {
                returnValue = "#Circular";
                cell.SetValue(returnValue);
            }
            catch(Exception)
            {
                returnValue = "#Error";
                cell.SetValue(returnValue);
            }
            finally
            {
                FormulaParser.Clear();
            }

            return returnValue;
        }

        /// <summary>
        /// Retrieves cell literal, if it exists.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <returns>Cell literal, if any.</returns>
        public string GetCellLiteral(string cellName)
        {
            Cell cell = GetCellFromDictionary(cellName);
            return cell != null ? cell.CellLiteral : "";
        }

        /// <summary>
        /// Inserts value to given cell.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <param name="value">Cell value.</param>
        public void PutCell(string cellName, string value)
        {
            PutCellToDictionary(cellName, value);
        }

        /// <summary>
        /// Retrieves the list of cells.
        /// </summary>
        /// <returns></returns>
        public List<Cell> GetCells()
        {
            return cells.Values.ToList();
        }

        #region Private methods

        /// <summary>
        /// Get a value stored in the cell dictionary, with validation of cellName.
        /// </summary>
        /// <param name="cellDictionary">Cell dictionary to look into.</param>
        /// <param name="cellName">Cell name.</param>
        /// <returns>Value, if found in dictionary.</returns>
        private Cell GetCellFromDictionary(string cellName)
        {
            if (!Cell.IsCellNameValid(cellName))
            {
                throw new ArgumentException(string.Format("{0} is not a valid cell name", cellName));
            }

            cells.TryGetValue(key: cellName, value: out Cell cellObject);

            return cellObject;
        }

        /// <summary>
        /// Put value to cell dictionary, with cellName validation.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <param name="cellLiteral">Cell literal to store the info in.</param>
        private void PutCellToDictionary(string cellName, string cellLiteral)
        {
            Cell existingCell = GetCellFromDictionary(cellName);
            
            if (existingCell != null)
            {
                existingCell.CellLiteral = cellLiteral;
            }
            else
            {
                cells[cellName] = new Cell(cellName, cellLiteral);
            }
        }

        #endregion
    }
}
