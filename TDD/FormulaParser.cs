﻿using System;
using System.Collections.Generic;

namespace TDD
{
    /// <summary>
    /// Formula parser implementation.
    /// Following grammar:
    ///     expr -> term {add-op term}
    ///     term -> factor {mul-op factor}
    ///     factor -> "(" expr ")" | number
    ///     add-op -> "+" | "-"
    ///     mul-op -> "*" | "/"
    ///     number -> digitseq
    ///     digitseq -> digit {digitseq} | ""
    ///     digit -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
    /// </summary>
    public class FormulaParser
    {
        /// <summary>
        /// Tokenizer used by the parser.
        /// </summary>
        private Tokenizer tokenizer;

        /// <summary>
        /// Stack of visited cells. If cell is visited again, and is on-stack, it's a circular dependency.
        /// </summary>
        private static Stack<string> visitedCells = new Stack<string>();

        /// <summary>
        /// Public method invoked externally to cause parsing of an expression.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        
        public string Parse(string input)
        { 
            // The formula should be parsed and evaluated only if the first
            // character is an equality sign.
            //
            if (String.IsNullOrEmpty(input) || !input.StartsWith("="))
            {
                return input;
            }

            tokenizer = new Tokenizer(input.Substring(1));

            string value = EvaluateExpression().ToString();

            // Valid grammar needs to have all tokens consumed.
            //
            if (tokenizer.PeekNextToken() != null)
            {
                visitedCells.Clear();
                throw new ArgumentException("Invalid grammar");
            }

            return value;
        }

        /// <summary>
        /// Parse the cell input for references.
        /// </summary>
        /// <param name="cellName">Name of the cell being parsed.</param>
        /// <param name="input">Input literal.</param>
        public void ParseReferences(string cellName, string input)
        {
            // The formula should be parsed and evaluated only if the first
            // character is an equality sign.
            //
            if (String.IsNullOrEmpty(input) || !input.StartsWith("="))
            {
                return;
            }

            tokenizer = new Tokenizer(input.Substring(1));

            string token;
            while((token = tokenizer.GetNextToken()) != null)
            {
                if (Cell.IsCellNameValid(token))
                {
                    ReferenceResolver.GetInstance().AddReference(cellName, token);
                }
            }
        }

        /// <summary>
        /// Evaluates expression as expr -> term { add-op term }
        /// </summary>
        /// <returns>Evaluated expression</returns>
        private int EvaluateExpression()
        {
            int value = EvaluateTerm();

            while (tokenizer.PeekNextToken() == "+" ||
                tokenizer.PeekNextToken() == "-")
            {
                string op = tokenizer.GetNextToken();
                if (op == "+")
                {
                    value += EvaluateTerm();
                }
                else
                {
                    value -= EvaluateTerm();
                }
            }

            return value;
        }

        /// <summary>
        /// Evaluate term based on grammar: term -> factor {mul-op factor}
        /// </summary>
        /// <returns>Evaluated term.</returns>
        private int EvaluateTerm()
        {
            int value = EvaluateFactor();

            while (tokenizer.PeekNextToken() == "*" ||
                tokenizer.PeekNextToken() == "/")
            {
                string op = tokenizer.GetNextToken();
                if (op == "*")
                {
                    value *= EvaluateFactor();
                }
                else
                {
                    value /= EvaluateFactor();
                }
            }

            return value;
        }

        /// <summary>
        /// Evaluates factor based on grammar factor -> "(" expr ")" | number
        /// </summary>
        /// <returns>Value of factor</returns>
        private int EvaluateFactor()
        {
            int value;

            if (tokenizer.PeekNextToken() == "(")
            {
                // Remove opening parentheses.
                //
                tokenizer.GetNextToken();

                value = EvaluateExpression();

                // Remove closing parentheses.
                //
                string token = tokenizer.GetNextToken();
                if (token != ")")
                {
                    throw new ArgumentException(string.Format("Invalid token at {0}", tokenizer.ParsedTokens));
                }
            }
            else
            {
                string token = tokenizer.GetNextToken();

                if (Cell.IsCellNameValid(token))
                {
                    if (visitedCells.Contains(token))
                    {
                        throw new CircularDependencyException(token);
                    }

                    visitedCells.Push(token);
                    token = ReferenceResolver.GetInstance().ResolveReference(token);

                    visitedCells.Pop();
                }

                value = int.Parse(token);
            }

            return value;
        }

        /// <summary>
        /// Clears static data of the parser.
        /// </summary>
        public static void Clear()
        {
            visitedCells.Clear();
        }

        /// <summary>
        /// Exception thrown when Circular dependency is detected.
        /// </summary>
        internal class CircularDependencyException : Exception
        {
            public string CellName
            {
                get;
                private set;
            }

            public CircularDependencyException(string cellName)
            {
                this.CellName = cellName;
            }

            public override string Message
            {
                get
                {
                    return string.Format("Circular dependency accessing cell {0}", CellName);
                }
            }
        }
    }
}
