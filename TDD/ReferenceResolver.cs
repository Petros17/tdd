﻿using System.Collections.Generic;

namespace TDD
{
    /// <summary>
    /// Class representing ReferenceResolver, used by the parser to resolve cell references.
    /// </summary>
    internal class ReferenceResolver
    {
        /// <summary>
        /// Singleton instance.
        /// </summary>
        private static ReferenceResolver _instance;

        /// <summary>
        /// Keeps list of references.
        /// </summary>
        private Dictionary<Cell, List<Cell>> referencesList = new Dictionary<Cell, List<Cell>>();
        private Dictionary<Cell, List<Cell>> referencedByList = new Dictionary<Cell, List<Cell>>();

        /// <summary>
        /// Singleton accessor.
        /// </summary>
        /// <returns></returns>
        public static ReferenceResolver GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ReferenceResolver();
            }

            return _instance;
        }

        /// <summary>
        /// Sheet this resolver is working with.
        /// </summary>
        public Sheet Sheet
        {
            get; set;
        }

        /// <summary>
        /// Resolves reference by querying the sheet.
        /// </summary>
        /// <param name="cellName">Name of the cell.</param>
        /// <returns>Cell value.</returns>
        public string ResolveReference(string cellName)
        {
            Cell cell = Sheet.GetCellObject(cellName);
            if (cell == null)
            {
                return "";
            }

            return cell.GetValue();
        }

        /// <summary>
        /// Adds reference from cell A to cell B.
        /// </summary>
        /// <param name="cellThatReferences">Cell which makes the reference</param>
        /// <param name="cellBeingReferenced">Cell being referenced</param>
        public void AddReference(string cellThatReferences, string cellBeingReferenced)
        {
            Cell referencingCell = Sheet.GetCellObject(cellThatReferences);
            Cell referencedCell = Sheet.GetCellObject(cellBeingReferenced);

            if (referencingCell == null || referencedCell == null)
            {
                return;
            }

            if (!referencesList.TryGetValue(referencingCell, out List<Cell> listOfReferences))
            {
                listOfReferences = new List<Cell>();
                referencesList.Add(referencingCell, listOfReferences);
            }

            // Add reference to the list of cells that cell A references.
            //
            if (!listOfReferences.Contains(referencedCell))
            {
                listOfReferences.Add(referencedCell);
            }

            if (!referencedByList.TryGetValue(referencedCell, out List<Cell> listOfReferencedBy))
            {
                listOfReferencedBy = new List<Cell>();
                referencedByList.Add(referencedCell, listOfReferencedBy);
            }

            // Add reference to the list of cells that are referencing cell B.
            //
            if (!listOfReferencedBy.Contains(referencingCell))
            {
                listOfReferencedBy.Add(referencingCell);
            }
        }

        /// <summary>
        /// Removes all references created by the cell. Done when cell literal is changed.
        /// </summary>
        /// <param name="cellThatReferenced">Name of the cell</param>
        public void RemoveReferences(string cellThatReferenced)
        {
            Cell cell = Sheet.GetCellObject(cellThatReferenced);
            if (cell == null)
            {
                return;
            }

            // Find all cells B which are being references by the passed cell, and remove information
            // that passed cell has ever referenced them.
            //
            if (referencesList.TryGetValue(cell, out List<Cell> listOfReferences))
            {
                foreach(var reference in listOfReferences)
                {
                    if (referencedByList.TryGetValue(reference, out List<Cell> listOfReferencedBy))
                    {
                        listOfReferencedBy.Remove(cell);
                    }
                }
            }
        }

        /// <summary>
        /// Notify all cells which referenced this cell that a value has changed.
        /// </summary>
        /// <param name="changedCell">Name of the changed cell.</param>
        public void NotifyReferences(string changedCell)
        {
            Cell changedCellObject = Sheet.GetCellObject(changedCell);

            if (changedCellObject == null)
            {
                return;
            }

            if (referencedByList.TryGetValue(changedCellObject, out List<Cell> listOfReferencedBy))
            {
                foreach(var reference in listOfReferencedBy)
                {
                    reference.UpdateCellValue();
                }
            }
        }
    }
}
