﻿using NUnit.Framework;

namespace TDD.Test
{
    /// <summary>
    /// Test class for SheetTableModel.
    /// </summary>
    [TestFixture]
    class SheetTableModelTest
    {
        private Sheet sheet;
        private SheetTableModel table;

        /// <summary>
        /// Setup run before each test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            sheet = new Sheet();
            table = new SheetTableModel(sheet);
        }

        #region Tests

        /// <summary>
        /// Test default table model methods.
        /// </summary>
        [Test]
        public void TestBasicTableModelMethods()
        {
            // Hardcoded values, expect at least 100*50 tables.
            //
            Assert.IsTrue(table.GetColumnCount() > 49);
            Assert.IsTrue(table.GetRowCount() > 99);
            Assert.AreEqual("", table.GetValueAt(10, 10));
        }

        /// <summary>
        /// Test that cellname getters work.
        /// </summary>
        [Test]
        public void TestColumnNameGetters()
        {
            Assert.AreEqual("", table.GetColumnName(0));
            Assert.AreEqual("A", table.GetColumnName(1));
            Assert.AreEqual("Z", table.GetColumnName(26));
            Assert.AreEqual("AZ", table.GetColumnName(26 + 26));
            Assert.AreEqual("ZZ", table.GetColumnName(26 + 26 * 26));
        }

        /// <summary>
        /// Verify that column 0 contains indexes of the rows.
        /// </summary>
        [Test]
        public void TestColumn0ContainsIndex()
        {
            Assert.AreEqual(1, table.GetValueAt(0, 0));
            Assert.AreEqual(50, table.GetValueAt(49, 0));
            Assert.AreEqual(100, table.GetValueAt(99, 0));
        }

        /// <summary>
        /// Test that columns have appropriate values stored in them.
        /// </summary>
        [Test]
        public void TestThatColumnsHaveValues()
        {
            sheet.PutCell("A1", "upper left");
            Assert.AreEqual("upper left", table.GetValueAt(0, 1));

            sheet.PutCell("A100", "a100 value");
            Assert.AreEqual("a100 value", table.GetValueAt(99, 1));

            sheet.PutCell("ZZ1", "upper right");
            Assert.AreEqual("upper right", table.GetValueAt(0, 26 + 26 * 26));

            sheet.PutCell("ZZ100", "zz100 value");
            Assert.AreEqual("zz100 value", table.GetValueAt(99, 26 + 26 * 26));
        }

        /// <summary>
        /// Verify that values can be stored to sheet via table model.
        /// </summary>
        [Test]
        public void TestThatStoreWorksThroughTableModel()
        {
            table.SetValueAt(0, 1, "21");
            table.SetValueAt(1, 1, "=A1");

            Assert.AreEqual("21", table.GetValueAt(0, 1));
            Assert.AreEqual("21", table.GetValueAt(1, 1));

            table.SetValueAt(0, 1, "22");
            Assert.AreEqual("22", table.GetValueAt(0, 1));
            Assert.AreEqual("22", table.GetValueAt(1, 1));
        }

        #endregion
    }
}
