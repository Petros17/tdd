﻿/**
* File containing unit tests for FormulaParser.
* 
* @author Petar Kovacev
* 
*/

using System;
using NUnit.Framework;

namespace TDD.Test
{
    /// <summary>
    /// Test class for testing tokenizer and formula parser.
    /// </summary>
    [TestFixture]
    public class FormulaParserTest
    {
        private Sheet sheet;

        [SetUp]
        public void SetUp()
        {
            sheet = new Sheet();
        }

        /// <summary>
        /// Test that tokenizer properly returns tokens.
        /// </summary>
        [Test]
        public void TokenizerSanityTest()
        {
            string input = "(123 )";
            Tokenizer tokenizer = new Tokenizer(input);

            Assert.AreEqual("(", tokenizer.GetNextToken());
            Assert.AreEqual("123", tokenizer.GetNextToken());
            Assert.AreEqual(")", tokenizer.GetNextToken());
            Assert.AreEqual(null, tokenizer.GetNextToken());
        }

        /// <summary>
        /// Verify that parsing empty or null string returns unchanged value.
        /// </summary>
        [Test]
        public void TestParsingEmptyOrNullInput()
        {
            FormulaParser parser = new FormulaParser();

            Assert.AreEqual("", parser.Parse(""), "Expected empty string as output of parsing an empty string");
            Assert.IsNull(parser.Parse(null), "Expected null as output of parsing null");
        }

        /// <summary>
        /// Verify that parsing non-formula input returns unchanged value.
        /// </summary>
        [Test]
        public void TestParsingNonFormulaInput()
        {
            FormulaParser parser = new FormulaParser();

            Assert.AreEqual("7+2+3", parser.Parse("7+2+3"), "Expected unchanged value after parsing");
        }

        /// <summary>
        /// Test formula spec, by adding trailing space before 'equals' mark.
        /// </summary>
        [Test]
        public void TestFormulaSpec()
        {
            string cellName = "B1";

            sheet.PutCell(cellName, " =7"); // note leading space
            Assert.AreEqual(" =7", sheet.GetCell(cellName), "Not a formula");
            Assert.AreEqual(" =7", sheet.GetCellLiteral(cellName), "Unchanged");
        }

        /// <summary>
        /// Test that formula containing only a single constant is properly evaluated.
        /// </summary>
        [Test]
        public void TestConstantFormula()
        {
            string cellName = "C1";

            sheet.PutCell(cellName, "=7");
            Assert.AreEqual("7", sheet.GetCell(cellName), "Expected evaluated constant formula");
            Assert.AreEqual("=7", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Basic parentheses test for constant formula.
        /// </summary>
        [Test]
        public void TestParentheses()
        {
            string cellName = "C1";

            sheet.PutCell(cellName, "=(((7)))");
            Assert.AreEqual("7", sheet.GetCell(cellName), "Expected evaluated constant formula");
            Assert.AreEqual("=(((7)))", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Verify that invalid formula for parentheses is properly errored.
        /// </summary>
        [Test]
        public void Negative_TestInvalidParenthesesGrammar()
        {
            string cellName = "C1";

            // Test multiple ending parentheses.
            //
            sheet.PutCell(cellName, "=(7))))))");
            Assert.AreEqual("#Error", sheet.GetCell(cellName), "Expected error as cell value");
            Assert.AreEqual("=(7))))))", sheet.GetCellLiteral(cellName));

            // Test numbers after closing parentheses.
            //
            sheet.PutCell(cellName, "=((7)1)");
            Assert.AreEqual("#Error", sheet.GetCell(cellName), "Expected error as cell value");
            Assert.AreEqual("=((7)1)", sheet.GetCellLiteral(cellName));

            // Test spaces between numbers inside parentheses.
            //
            sheet.PutCell(cellName, "=((7 1))");
            Assert.AreEqual("#Error", sheet.GetCell(cellName), "Expected error as cell value");
            Assert.AreEqual("=((7 1))", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Test addition.
        /// </summary>
        [Test]
        public void TestAdd()
        {
            string cellName = "C1";

            // Test basic addition.
            //
            sheet.PutCell(cellName, "=2+3");
            Assert.AreEqual("5", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=2+3", sheet.GetCellLiteral(cellName));

            // Test addition with multiple numbers.
            //
            sheet.PutCell(cellName, "=1+2+3+4");
            Assert.AreEqual("10", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=1+2+3+4", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Test subtraction.
        /// </summary>
        [Test]
        public void TestSub()
        {
            string cellName = "C1";

            // Test basic subtraction.
            //
            sheet.PutCell(cellName, "=3-2");
            Assert.AreEqual("1", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=3-2", sheet.GetCellLiteral(cellName));

            // Test subtraction with multiple numbers.
            //
            sheet.PutCell(cellName, "=6-3-2");
            Assert.AreEqual("1", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=6-3-2", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Test combined addition and subtraction with parentheses.
        /// </summary>
        [Test]
        public void TestAddAndSubWithParentheses()
        {
            string cellName = "C1";

            // Test complicated addition and subtraction.
            //
            sheet.PutCell(cellName, "=7-((3+1)-2-(4+3))+(10-1)");
            Assert.AreEqual("21", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=7-((3+1)-2-(4+3))+(10-1)", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Basic MUL evaluation test.
        /// </summary>
        [Test]
        public void TestMul()
        {
            string cellName = "D2";

            sheet.PutCell(cellName, "=2*3*4");
            Assert.AreEqual("24", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=2*3*4", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Basic DIV evaluation test.
        /// </summary>
        [Test]
        public void TestDiv()
        {
            string cellName = "D2";

            sheet.PutCell(cellName, "=12/6");
            Assert.AreEqual("2", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=12/6", sheet.GetCellLiteral(cellName));

            sheet.PutCell(cellName, "=12/0");
            Assert.AreEqual("#Error", sheet.GetCell(cellName), "Expected error on division with zero.");
            Assert.AreEqual("=12/0", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Sanity precedence test, based on addition and multiplication.
        /// </summary>
        [Test]
        public void SanityPrecedenceTest()
        {
            string cellName = "E5";

            sheet.PutCell(cellName, "=3+2*5");
            Assert.AreEqual("13", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("=3+2*5",
                sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Testing full expression with all operations.
        /// </summary>
        [Test]
        public void FullExpressionTest()
        {
            string cellName = "D12";

            sheet.PutCell(cellName, "= 7 + (4*3 - 6/3 + 15 - 14/2) / 9 * (3 + 12 / 2 - 5)");
            Assert.AreEqual("15", sheet.GetCell(cellName), "Expected evaluated expression.");
            Assert.AreEqual("= 7 + (4*3 - 6/3 + 15 - 14/2) / 9 * (3 + 12 / 2 - 5)",
                sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Test that invalid grammar will cause #Error on evaluation.
        /// </summary>
        [Test]
        public void Negative_TestInvalidExpression()
        {
            string cellName = "H15";

            sheet.PutCell(cellName, "=7*");
            Assert.AreEqual("#Error", sheet.GetCell(cellName), "Expected error.");
            Assert.AreEqual("=7*", sheet.GetCellLiteral(cellName));
        }

        /// <summary>
        /// Verify that reference to another cell
        /// </summary>
        [Test]
        public void TestCellReference()
        {
            sheet.PutCell("A1", "8");
            sheet.PutCell("A2", "=A1");
            Assert.AreEqual("8", sheet.GetCell("A2"));
        }

        /// <summary>
        /// Verify that changing a cell changes values of all cells that reference it.
        /// </summary>
        [Test]
        public void TestCellValuePropagation()
        {
            sheet.PutCell("A1", "8");
            sheet.PutCell("A2", "=A1");
            Assert.AreEqual("8", sheet.GetCell("A2"));

            sheet.PutCell("A1", "9");
            Assert.AreEqual("9", sheet.GetCell("A2"), "Expected propagation of cell value.");
        }

        /// <summary>
        /// Verify that formulas containing cell references are properly evaluated.
        /// </summary>
        [Test]
        public void TestFormulasWithCellReferences()
        {
            sheet.PutCell("A1", "8");
            sheet.PutCell("A2", "3");
            sheet.PutCell("B1", "=A1*(A1-A2)+A2/3");
            Assert.AreEqual("41", sheet.GetCell("B1"), "Expected calculated value of 41");
        }

        /// <summary>
        /// Test that multiple levels of cell reference works.
        /// </summary>
        [Test]
        public void TestThatDeepPropagationWorks()
        {
            sheet.PutCell("A1", "8");
            sheet.PutCell("A2", "=A1");
            sheet.PutCell("A3", "=A2");
            sheet.PutCell("A4", "=A3");
            Assert.AreEqual("8", sheet.GetCell("A4"));

            sheet.PutCell("A2", "6");
            Assert.AreEqual("6", sheet.GetCell("A4"));
        }

        /// <summary>
        /// Test formula with multiple dependencies.
        /// </summary>
        [Test]
        public void TestThatFormulaWorksWithManyCells()
        {
            sheet.PutCell("A1", "10");
            sheet.PutCell("A2", "=A1+B1");
            sheet.PutCell("A3", "=A2+B2");
            sheet.PutCell("A4", "=A3");
            sheet.PutCell("B1", "7");
            sheet.PutCell("B2", "=A2");
            sheet.PutCell("B3", "=A3-A2");
            sheet.PutCell("B4", "=A4+B3");

            Assert.AreEqual("34", sheet.GetCell("A4"));
            Assert.AreEqual("51", sheet.GetCell("B4"));
        }

        /// <summary>
        /// Test that basic circular reference is properly handled.
        /// </summary>
        [Test]
        public void SanityTestCircularReference()
        {
            sheet.PutCell("A1", "=A1");
            Assert.AreEqual("#Circular", sheet.GetCell("A1"), "Expected error for Circular dependency");
        }

        /// <summary>
        /// Verify that more complex circular dependency is detected.
        /// </summary>
        [Test]
        public void ComplexCircularReferenceTest()
        {
            sheet.PutCell("A1", "=A2 + A3");
            sheet.PutCell("A2", "=A3 * A4");
            sheet.PutCell("A3", "=A1 / A2");
            sheet.PutCell("A4", "7");

            Assert.AreEqual("#Circular", sheet.GetCell("A1"), "Expected error for Circular dependency");
            Assert.AreEqual("#Circular", sheet.GetCell("A2"), "Expected error for Circular dependency");
            Assert.AreEqual("#Circular", sheet.GetCell("A3"), "Expected error for Circular dependency");
        }
    }
}
