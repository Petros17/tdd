﻿using NUnit.Framework;
using TDD.GUI;

namespace TDD.Test
{
    /// <summary>
    /// Unit tests for the GUI.
    /// </summary>
    [TestFixture]
    public class GuiTest
    {
        private SheetTableModel sheetTableModel;
        private GuiForm guiForm;

        /// <summary>
        /// Set up method, ran before each test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.sheetTableModel = new SheetTableModel();
            this.guiForm = new GuiForm(sheetTableModel);
            this.guiForm.Show();
        }

        /// <summary>
        /// Verify that basic form parts are present after initializing.
        /// </summary>
        [Test]
        public void TestFormHasRightParts()
        {
            Assert.IsNotNull(guiForm.dataGridView);
            Assert.IsNotNull(guiForm.cellNameLabel);
            Assert.IsNotNull(guiForm.cellLiteralTextbox);
            Assert.IsNotNull(guiForm.okButton);
            Assert.AreEqual(sheetTableModel, guiForm.sheetTableModel);

            Assert.AreEqual(System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect,
                guiForm.dataGridView.SelectionMode);
        }

        /// <summary>
        /// Verify that cell name label is updated once a selection change occurs.
        /// </summary>
        [Test]
        public void TestLabelUpdatesOnSelection()
        {    
            guiForm.dataGridView.CurrentCell = guiForm.dataGridView[0, 0];
            Assert.AreEqual("A1", guiForm.cellNameLabel.Text);

            guiForm.dataGridView.CurrentCell = guiForm.dataGridView[0, 5];
            Assert.AreEqual("A6", guiForm.cellNameLabel.Text);
        }

        /// <summary>
        /// Verify that the cell literal editor sees proper literal from the model.
        /// </summary>
        [Test]
        public void TestThatEditorSeesLiteral()
        {
            sheetTableModel.SetValueAt(1, 1, "=7");
            guiForm.dataGridView.CurrentCell = guiForm.dataGridView[0, 1];
            Assert.AreEqual("=7", guiForm.cellLiteralTextbox.Text);
        }

        /// <summary>
        /// Verify that edited cell literal gets saved.
        /// </summary>
        [Test]
        public void TestThatEditedValueIsSaved()
        {
            sheetTableModel.SetValueAt(0, 1, "=7");
            guiForm.dataGridView.CurrentCell = guiForm.dataGridView[0, 0];

            guiForm.cellLiteralTextbox.Text = "=8";
            guiForm.okButton.PerformClick();

            Assert.AreEqual("8", sheetTableModel.GetValueAt(0, 1));
            Assert.AreEqual("=8", sheetTableModel.GetLiteralValueAt(0, 1));
        }

        /// <summary>
        /// Test that value propagation (via cell reference) works.
        /// </summary>
        [Test]
        public void TestThatValuePropagationWorks()
        {
            sheetTableModel.SetValueAt(0, 1, "=7");
            sheetTableModel.SetValueAt(1, 1, "=A1 + 2");

            guiForm.dataGridView.CurrentCell = guiForm.dataGridView[0, 0];

            guiForm.cellLiteralTextbox.Text = "=8";
            guiForm.okButton.PerformClick();

            Assert.AreEqual("8", sheetTableModel.GetValueAt(0, 1));
            Assert.AreEqual("10", sheetTableModel.GetValueAt(1, 1));
        }
    }
}
