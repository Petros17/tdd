﻿/**
* File containing unit tests for Sheet.
* 
* @author Petar Kovacev
* 
*/

using System;
using NUnit.Framework;

namespace TDD.Test
{
    /// <summary>
    /// Test functionality of Sheet class.
    /// </summary>
    [TestFixture]
    public class SheetTest
    {
        private Sheet sheet;

        /// <summary>
        /// Setup, run before each test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            sheet = new Sheet();
        }

        #region Tests

        /// <summary>
        /// Test that cells of empty sheet are also empty.
        /// </summary>
        [Test]
        public void TestThatCellsAreEmptyByDefault()
        {
            Random random = new Random();

            // Test 10 random cell names.
            //
            for (int i = 0; i < 10; i++)
            {
                string cellName = GenerateRandomCellName(random);
                string message = string.Format("Expected cell {0} to be empty.", cellName);

                Assert.AreEqual("", sheet.GetCell(cellName), message);
            }
        }

        /// <summary>
        /// Test that non-existant cells are not parsed and throw exception.
        /// </summary>
        [Test]
        public void Negative_TestNonExistantCells()
        {
            // Verify that cells starting with numbers are invalid.
            //
            Assert.Throws(typeof(ArgumentException), () => sheet.GetCell("123ABC"));
            Assert.Throws(typeof(ArgumentException), () => sheet.PutCell("123ABC", "value"));

            // Verify that cells starting with three letters are invalid.
            //
            Assert.Throws(typeof(ArgumentException), () => sheet.GetCell("ABC123"));
            Assert.Throws(typeof(ArgumentException), () => sheet.PutCell("ABC123", "value"));

            // Verify that cells containing letters after numbers are invalid.
            //
            Assert.Throws(typeof(ArgumentException), () => sheet.GetCell("AB12A"));
            Assert.Throws(typeof(ArgumentException), () => sheet.PutCell("AB12A", "value"));

            // Verify that cells where numbers start with 0 are invalid.
            //
            Assert.Throws(typeof(ArgumentException), () => sheet.GetCell("AB012"));
            Assert.Throws(typeof(ArgumentException), () => sheet.PutCell("AB012", "value"));

            // Verify that cells with no numbers are invalid.
            //
            Assert.Throws(typeof(ArgumentException), () => sheet.GetCell("AB"));
            Assert.Throws(typeof(ArgumentException), () => sheet.PutCell("AB", "value"));
        }

        /// <summary>
        /// Test that cells are properly stored.
        /// </summary>
        [Test]
        public void TestThatCellsAreStored()
        {
            string cellName = "A1";

            sheet.PutCell(cellName, "Cell value on A1");
            Assert.AreEqual("Cell value on A1", sheet.GetCell(cellName));

            sheet.PutCell(cellName, "Different value on A1");
            Assert.AreEqual("Different value on A1", sheet.GetCell(cellName));

            sheet.PutCell(cellName, "");
            Assert.AreEqual("", sheet.GetCell(cellName));
        }

        /// <summary>
        /// Test that various cells exist at the same time.
        /// </summary>
        [Test]
        public void TestThatManyCellsExist()
        {
            Random random = new Random();
            string firstCellName = GenerateRandomCellName(random);
            string secondCellName = GenerateRandomCellName(random);
            string thirdCellName = GenerateRandomCellName(random);

            // Populate cells, and verify that values are proper.
            //
            sheet.PutCell(firstCellName, "First");
            sheet.PutCell(secondCellName, "Second");
            sheet.PutCell(thirdCellName, "Third");

            Assert.AreEqual("First", sheet.GetCell(firstCellName));
            Assert.AreEqual("Second", sheet.GetCell(secondCellName));
            Assert.AreEqual("Third", sheet.GetCell(thirdCellName));

            // Change value of first cell, and verify it's value changed,
            // but remaining values remained the same.
            //
            sheet.PutCell(firstCellName, "First after changing");
            Assert.AreEqual("First after changing", sheet.GetCell(firstCellName));
            Assert.AreEqual("Second", sheet.GetCell(secondCellName));
            Assert.AreEqual("Third", sheet.GetCell(thirdCellName));
        }

        /// <summary>
        /// Test that cells with numeric values are properly identified, and stored with extra spaces 
        /// removed.
        /// </summary>
        [Test]
        public void TestThatNumericCellsAreIdentifiedAndStored()
        {
            string cellName = "A21";

            // Put generic string, containing a number.
            //
            sheet.PutCell(cellName, "X99");
            Assert.AreEqual("X99", sheet.GetCell(cellName));

            // Regular number.
            //
            sheet.PutCell(cellName, "14");
            Assert.AreEqual("14", sheet.GetCell(cellName));

            // String with extra space, but not entierly numeric.
            //
            sheet.PutCell(cellName, " 99 X");
            Assert.AreEqual(" 99 X", sheet.GetCell(cellName));

            // Regular number, with extra spaces. They should be removed.
            //
            sheet.PutCell(cellName, " 1234 ");
            Assert.AreEqual("1234", sheet.GetCell(cellName));

            // Just a blank. It should preserve value, given that there is no number
            // involved.
            //
            sheet.PutCell(cellName, " ");
            Assert.AreEqual(" ", sheet.GetCell(cellName));
        }

        /// <summary>
        /// Test access to literals representing cell values.
        /// </summary>
        [Test]
        public void TestAccessToCellLiterals()
        {
            string cellName = "A21";

            sheet.PutCell(cellName, "Some string");
            Assert.AreEqual("Some string", sheet.GetCellLiteral(cellName));

            sheet.PutCell(cellName, " 1234 ");
            Assert.AreEqual(" 1234 ", sheet.GetCellLiteral(cellName));

            sheet.PutCell(cellName, "=7");
            Assert.AreEqual("=7", sheet.GetCellLiteral(cellName));
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Generates a random cell name.
        /// </summary>
        /// <param name="random">Random generator.</param>
        /// <returns>Generated cell name.</returns>
        private static string GenerateRandomCellName(Random random)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            const string numbers = "1234567890";

            char firstChar = chars[random.Next(chars.Length)];
            string result = chars[random.Next(chars.Length)].ToString();

            if(random.NextDouble() > 0.5)
            {
                result += chars[random.Next(chars.Length)].ToString();
            }

            // Add first number, without the 0, since it's invalid.
            //
            result += numbers[random.Next(numbers.Length - 1)].ToString();

            // Add random amount of numbers.
            //
            for (int i = 0; i < random.NextDouble() * 10; i++)
            {
                result += numbers[random.Next(numbers.Length)].ToString();
            }

            return result;
        }

        #endregion
    }
}
