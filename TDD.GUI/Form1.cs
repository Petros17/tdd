﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace TDD.GUI
{
    public partial class GuiForm : Form
    {
        /// <summary>
        /// Public constructor.
        /// </summary>
        public GuiForm()
        {
            InitializeComponent();
            this.sheetTableModel = new SheetTableModel();

            InitializeDataSources();
            InitializeListeners();

            dataGridView.CurrentCell = null;
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="sheetTableModel">Sheet table model.</param>
        public GuiForm(SheetTableModel sheetTableModel)
        {
            this.sheetTableModel = sheetTableModel;
            InitializeComponent();
            InitializeDataSources();
            InitializeListeners();

            dataGridView.CurrentCell = null;
        }

        /// <summary>
        /// Initializes the data sources of the gui.
        /// </summary>
        private void InitializeDataSources()
        {
            this.bindingSource = new BindingSource();
            bindingSource.DataSource = new BindingList<Cell>(sheetTableModel.GetCells());
            dataGridView.DataSource = bindingSource; 
        }

        /// <summary>
        /// Initializes the listeners of the elements.
        /// </summary>
        private void InitializeListeners()
        {
            dataGridView.SelectionChanged += new EventHandler(DataGridView_SelectionChanged);
            okButton.Click += new EventHandler(OKButton_Clicked);
        }

        /// <summary>
        /// Selection changed listener for the datagridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0 && dataGridView.SelectedRows[0].Cells.Count > 0)
            {
                cellNameLabel.Text = dataGridView.SelectedRows[0].Cells[0].Value.ToString();
                cellLiteralTextbox.Text = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
            }
            else
            {
                cellNameLabel.Text = "";
                cellLiteralTextbox.Text = "";
            }
        }

        /// <summary>
        /// Listener for OK button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Clicked(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0 && dataGridView.SelectedRows[0].Cells.Count > 0)
            {
                string cellname = dataGridView.SelectedRows[0].Cells[0].Value.ToString();  

                Cell cell = sheetTableModel.FindCell(cellname);                      
                cell.CellLiteral = cellLiteralTextbox.Text;
                dataGridView.UpdateCellValue(1, dataGridView.SelectedRows[0].Index);
                dataGridView.UpdateCellValue(2, dataGridView.SelectedRows[0].Index);
            }
        }

        public SheetTableModel sheetTableModel;
        public BindingSource bindingSource;
    }
}
